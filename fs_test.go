package matchedfs_test

import (
	"io"
	"log"
	"os"
	"path/filepath"
	"testing"
	"time"

	"gitlab.com/tbhartman/go-matchedfs"

	"github.com/spf13/afero"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

// new makes a matched fs with:
//  - /a/
//  - /a/a1/
//  - /a/a1/a1.txt   : read only
//  - /a/a2/         : excluded
//  - /a/a2/a2.txt
//  - /a/a3          : read only
//  - /a/a3/a3.1.txt
//  - /a/a3/a3.2.txt
//  - /a/a3/a3.3.txt : excluded
//  - /a/a3/a3.4.txt
//  - /b/            : write only
//  - /b/b.txt       : write only
func new() matchedfs.Fs {
	// the MemMapFs seems not to correctly handle bits;
	// when writing files, must chmod explicitly
	ufs := afero.NewMemMapFs()
	ufs.MkdirAll("a/a1", os.ModeDir|0700)
	ufs.MkdirAll("a/a2", os.ModeDir|0700)
	ufs.MkdirAll("a/a3", os.ModeDir|0700)
	ufs.MkdirAll("b", os.ModeDir|0700)

	// create files
	for _, s := range []string{
		"a/a1/a1.txt",
		"a/a2/a2.txt",
		"a/a3/a3.1.txt",
		"a/a3/a3.2.txt",
		"a/a3/a3.3.txt",
		"a/a3/a3.4.txt",
		"b/b.txt",
	} {
		fa, err := ufs.Create(s)
		if err != nil {
			log.Panic(err)
		}
		fa.WriteString(filepath.Base(s))
		fa.Close()
		ufs.Chmod(s, 0600)
	}

	fs := matchedfs.New(ufs)
	fs.SetExcludeMatcher(&myMatcher{[]string{"a/a2/", "a/a3/a3.3.txt"}})
	fs.AddPermissionMatcher(&myMatcher{[]string{"a/a1/a1.txt"}}, 0333)
	fs.AddPermissionMatcher(&myMatcher{[]string{"a/a3/"}}, 0222)
	fs.AddPermissionMatcher(&myMatcher{[]string{"b/"}}, 0555)
	fs.AddPermissionMatcher(&myMatcher{[]string{"b/b.txt"}}, 0555)

	return fs
}

func TestOpen(t *testing.T) {
	fs := new()

	_, err := fs.Open("b/b.txt")
	assert.Error(t, err)
	assert.True(t, os.IsPermission(err))

	_, err = fs.Open("b")
	assert.Error(t, err)
	assert.True(t, os.IsPermission(err))

	_, err = fs.Open("b/b.txt")
	assert.Error(t, err)
	assert.True(t, os.IsPermission(err))

	f, err := fs.Create("b/b.txt")
	assert.NoError(t, err)
	if assert.NotNil(t, f) {
		f.WriteString("test")
		f.Close()
	}

	f, err = fs.Open("a/a1/a1.txt")
	require.NoError(t, err)
	_, err = f.WriteString("")
	assert.Error(t, err)
	err = f.Truncate(0)
	assert.Error(t, err)

}

func TestStat(t *testing.T) {
	fs := new()

	s, err := fs.Stat("a/a1/a1.txt")
	assert.NoError(t, err)
	if assert.NotNil(t, s) {
		assert.Nil(t, s.Sys())
		assert.True(t, s.Mode()&0400 > 0, "File must be readable")
		assert.True(t, s.Mode()&0200 == 0, "File must not be writable")
	}

	_, err = fs.Stat("this does not exist")
	assert.Error(t, err)
}

func TestCreate(t *testing.T) {
	fs := new()

	f, err := fs.Create("a/a1/a1.txt")
	if !assert.Error(t, err) {
		f.Close()
	}

	f, err = fs.Create("a/a2/another.txt")
	if !assert.Error(t, err) {
		f.Close()
	}

	f, err = fs.Create("a/a3/a_file_here")
	if !assert.Error(t, err) {
		f.Close()
	}

	f, err = fs.Create("a/a3/a3.1.txt")
	if assert.NoError(t, err) {
		f.Close()
	}

	f, err = fs.Create("a/a3/a3.3.txt")
	if !assert.Error(t, err) {
		f.Close()
	}
}

func TestFile(t *testing.T) {
	fs := new()

	f, err := fs.Create("a/a1/test.txt")
	require.NoError(t, err)
	f.WriteString("test")
	f.Close()

	f, err = fs.Open("a/a1/a1.txt")
	require.NoError(t, err)
	stat, err := f.Stat()
	require.NoError(t, err)
	f.Close()

	assert.True(t, stat.Mode()&0222 == 0, "File must not be writable")

	f, err = fs.Open("a/a3")
	require.NoError(t, err)
	defer f.Close()
	names, err := f.Readdirnames(1)
	assert.NoError(t, err)
	assert.EqualValues(t, []string{"a3.1.txt"}, names)
	names, err = f.Readdirnames(1)
	assert.NoError(t, err)
	assert.EqualValues(t, []string{"a3.2.txt"}, names)
	names, err = f.Readdirnames(1)
	assert.NoError(t, err)
	assert.EqualValues(t, []string{"a3.4.txt"}, names)
	names, err = f.Readdirnames(1)
	assert.Error(t, err)
	assert.Equal(t, io.EOF, err)
	assert.Nil(t, names)

	// test reading full directory
	f, err = fs.Open("a/a3")
	require.NoError(t, err)
	defer f.Close()
	names, err = f.Readdirnames(0)
	assert.NoError(t, err)
	assert.EqualValues(t, []string{"a3.1.txt", "a3.2.txt", "a3.4.txt"}, names)
}

// TestReaddirNoExcludes reproduces #1
func TestReaddirNoExcludes(t *testing.T) {
	fs := new()

	f, err := fs.Open("a/a1")
	require.NoError(t, err)
	defer f.Close()
	names, err := f.Readdirnames(0)
	assert.NoError(t, err)
	assert.EqualValues(t, []string{"a1.txt"}, names)
}

func TestMkdir(t *testing.T) {
	fs := new()

	// permission error due to read only mode
	err := fs.MkdirAll("a/a1/anotherdir/d/e/f/g", os.ModeDir|0444)
	assert.Error(t, err)
	assert.True(t, os.IsPermission(err), "%v", err)

	// correct MkdirAll
	err = fs.MkdirAll("a/a1/yetanotherdir/d/e/f/g", os.ModeDir|0700)
	assert.NoError(t, err)

	// do not allow due to exclude
	fs.SetExcludeMatcher(&myMatcher{[]string{"a/a1/athirdone/d/"}})
	err = fs.MkdirAll("a/a1/athirdone/d", os.ModeDir|0700)
	assert.Error(t, err)
	assert.True(t, os.IsPermission(err))

	// do not allow Mkdir due to exclude
	fs.SetExcludeMatcher(&myMatcher{[]string{"a/a1/d/"}})
	err = fs.Mkdir("a/a1/d", os.ModeDir|0700)
	assert.Error(t, err)
	assert.True(t, os.IsPermission(err))

	// do not allow mkdir when parent does not exist
	err = fs.Mkdir("a/a1/e/f", os.ModeDir|0700)
	assert.Error(t, err)
	assert.True(t, os.IsNotExist(err))
}

func TestChown(t *testing.T) {
	fs := new()

	err := fs.Chown("a/a3/a3.3.txt", 0, 0)
	assert.Error(t, err)
	assert.True(t, os.IsNotExist(err))

	err = fs.Chown("a/a3/a3.99.txt", 0, 0)
	assert.Error(t, err)
	assert.True(t, os.IsNotExist(err))

	err = fs.Chown("a/a3/a3.4.txt", 0, 0)
	assert.NoError(t, err)
}

func TestChmod(t *testing.T) {
	fs := new()

	err := fs.Chmod("a/a3/a3.3.txt", 0600)
	assert.Error(t, err)
	assert.True(t, os.IsNotExist(err))

	err = fs.Chmod("a/a3/a3.99.txt", 0600)
	assert.Error(t, err)
	assert.True(t, os.IsNotExist(err))

	err = fs.Chmod("a/a3/a3.4.txt", 0600)
	assert.NoError(t, err)
}

func TestChtimes(t *testing.T) {
	fs := new()
	now := time.Now()

	err := fs.Chtimes("a/a3/a3.3.txt", now, now)
	assert.Error(t, err)
	assert.True(t, os.IsNotExist(err))

	err = fs.Chtimes("a/a3/a3.99.txt", now, now)
	assert.Error(t, err)
	assert.True(t, os.IsNotExist(err))

	err = fs.Chtimes("a/a3/a3.4.txt", now, now)
	assert.NoError(t, err)
}

func TestRemove(t *testing.T) {
	fs := new()

	// file is readonly
	err := fs.Remove("a/a1/a1.txt")
	assert.Error(t, err)
	assert.True(t, os.IsPermission(err))

	// directory is read only
	err = fs.Remove("a/a3/a3.3.txt")
	assert.Error(t, err)
	assert.True(t, os.IsPermission(err))

	// directory is excluded
	err = fs.Remove("a/a2/a2.txt")
	assert.Error(t, err)
	assert.True(t, os.IsNotExist(err))

	// should be ok
	err = fs.Remove("b/b.txt")
	assert.NoError(t, err)
}
