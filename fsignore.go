package matchedfs

// PathMatcher replicates the interface used by
// "github.com/sabhiram/go-gitignore" .
type PathMatcher interface {
	MatchesPath(path string) bool
}

type pathMatcher struct {
	PathMatcher
}
func (p *pathMatcher) Match(path string) bool {
	return p.MatchesPath(path)
}

// MatcherFromPathMatcher converts the interface from
// "github.com/sabhiram/go-gitignore" to a Matcher as a convenience function.
func matcherFromPathMatcher(m PathMatcher) Matcher {
	return &pathMatcher{m}
}
