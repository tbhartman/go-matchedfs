Matched Fs
============

[![Go Reference](https://pkg.go.dev/badge/gitlab.com/tbhartman/go-matchedfs.svg)](https://pkg.go.dev/gitlab.com/tbhartman/go-matchedfs)
[![pipeline status](https://gitlab.com/tbhartman/go-matchedfs/badges/master/pipeline.svg)](https://gitlab.com/tbhartman/go-matchedfs/-/commits/master)
[![coverage report](https://gitlab.com/tbhartman/go-matchedfs/badges/master/coverage.svg)](https://gitlab.com/tbhartman/go-matchedfs/-/commits/master)
[![goreport](https://goreportcard.com/badge/gitlab.com/tbhartman/go-matchedfs)](https://goreportcard.com/report/gitlab.com/tbhartman/go-matchedfs)

This package implements an [`afero.Fs`](https://pkg.go.dev/github.com/spf13/afero#Fs)
that allows matcher functions to hide underlying paths or alter permissions of
underlying paths.

The interface adds two methods to the Fs:

  - [`SetExcludeMatcher`](https://pkg.go.dev/gitlab.com/tbhartman/go-matchedfs#SetExcludeMatcher); to hide files *à la* gitignore
  - [`AddPermissionMatcher`](https://pkg.go.dev/gitlab.com/tbhartman/go-matchedfs#AddPermissionMatcher); to mask permissions


```go
package matchedfs_test

import (
	"fmt"
	"os"

	"github.com/spf13/afero"
	"gitlab.com/tbhartman/go-matchedfs"
)

type myMatcher struct {
	matchString []string
}
func (m *myMatcher) Match(name string) bool {
	for _, str := range m.matchString {
		if name == str {
			return true
		}
	}
	return false
}

func ExampleFs() {
	fs := matchedfs.New(afero.NewMemMapFs())
	fs.Mkdir("a", os.ModeDir | 0700)
	fs.Mkdir("b", os.ModeDir | 0700)

	fs.SetExcludeMatcher(&myMatcher{[]string{"a/"}})

	_, errA := fs.Stat("a")
	if errA == nil {
		fmt.Println("a exists")
	} else {
		fmt.Println("a does not exist")
	}
	_, errB := fs.Stat("b")
	if errB == nil {
		fmt.Println("b exists")
	} else {
		fmt.Println("b does not exist")
	}

	// Output:
	// a does not exist
	// b exists
}
```
