// Package matchedfs allows to hide or alter permissions of an underlying
// afero.Fs.
package matchedfs

import (
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/spf13/afero"
)

// Matcher is the interface to match a path.  The path is passed through
// filepath.Clean and filepath.ToSlash.  If it matches an existing directory,
// a slash is appended.  The resulting path is passed to Match.
type Matcher interface {
	Match(path string) bool
}


// Fs exposes the underlying afero.Fs according to the exclude and
// permission matchers.  If the exclude matcher matches a path,
// associated operations return an os.ErrNotExists.  If one of the
// permission matchers masks in such a way as to not allow an operation,
// the associated operation returns os.ErrPermission.
type Fs interface {
	afero.Fs

	// SetExcludeMatcher sets the Matcher to be used for excluding/hiding
	// files from the filesystem.
	SetExcludeMatcher(Matcher)
	SetExcludePathMatcher(PathMatcher)

	// AddPermissionMatcher adds a Matcher which applies the os.FileMode mask
	// to the matched paths mode bits.  Specifically, a given file/directory
	// permissions are bitcleared by mode.
	AddPermissionMatcher(m Matcher, mode os.FileMode)
	AddPermissionPathMatcher(p PathMatcher, mode os.FileMode)
}

type permissionMatcher struct {
	Matcher
	Mask os.FileMode
}

type matchedfs struct {
	fs afero.Fs
	exclude Matcher
	perm []*permissionMatcher
}

func (m *matchedfs) SetExcludeMatcher(matcher Matcher) {
	m.exclude = matcher
}
func (m *matchedfs) SetExcludePathMatcher(matcher PathMatcher) {
	m.exclude = matcherFromPathMatcher(matcher)
}
func (m *matchedfs) AddPermissionMatcher(matcher Matcher, mode os.FileMode) {
	m.perm = append(
		m.perm,
		&permissionMatcher{
			Matcher: matcher,
			Mask: mode,
		},
	)
}
func (m *matchedfs) AddPermissionPathMatcher(matcher PathMatcher, mode os.FileMode) {
	m.AddPermissionMatcher(matcherFromPathMatcher(matcher), mode)
}

type checkMode os.FileMode

const (
	exists checkMode = 0
	writable checkMode = 0222
	readable checkMode = 0444
	executable checkMode = 0111
)

type checkResult struct {
	Excluded bool
	Exists bool
	IsDir bool
	Writable bool
	Readable bool
	Executable bool
	Permission os.FileMode
}

func (m *matchedfs) check(name string) (ret checkResult) {
	name = filepath.ToSlash(name)
	endsSlash := strings.HasSuffix(name, "/")
	name = filepath.Clean(name)
	name = filepath.ToSlash(name)
	stat, err := m.fs.Stat(name)
	if (err == nil && stat.IsDir()) || endsSlash {
		name += "/"
	}

	ret.Excluded = m.exclude != nil && m.exclude.Match(name)
	ret.Exists = err == nil
	ret.IsDir = err == nil && stat.IsDir()
	
	var perm os.FileMode
	if err == nil {
		perm = stat.Mode()
	} else {
		perm = os.ModeDir | 0777
	}
	for _, matcher := range m.perm {
		if matcher.Match(name) {
			perm &^= matcher.Mask
		}
	}
	ret.Readable = perm & 0444 != 0
	ret.Writable = perm & 0222 != 0
	ret.Executable = perm & 0111 != 0
	ret.Permission = perm
	return
}

func (m *matchedfs) convertFile(path string) func(f afero.File, err error) (afero.File, error) {
	return func(f afero.File, err error) (afero.File, error) {
		return &file{
			File: f,
			fs: m,
			Path: path,
		}, err
	}
}
func (m *matchedfs) convertStat(path string, r checkResult) func(stat os.FileInfo, err error) (os.FileInfo, error) {
	return func(stat os.FileInfo, err error) (os.FileInfo, error) {
		if err != nil {
			return nil, err
		}
		return &fileInfo{
			FileInfo: stat,
			Permission: r.Permission,
			}, nil
	}
}

func (m *matchedfs) Create(name string) (afero.File, error) {
	parent := m.check(filepath.Dir(name))
	current := m.check(name)
	switch {
	case !parent.Exists || parent.Excluded:
		return nil, os.ErrNotExist
	case !current.Exists && !parent.Writable:
		return nil, os.ErrPermission
	case current.Excluded:
		return nil, os.ErrNotExist
	case !current.Writable:
		return nil, os.ErrPermission
	default:
		return m.convertFile(name)(m.fs.Create(name))
	}
}

func (m *matchedfs) Chown(name string, uid, gid int) error {
	current := m.check(name)
	switch {
	case !current.Exists || current.Excluded:
		return os.ErrNotExist
	default:
		return m.fs.Chown(name, uid, gid)
	}
}

func (m *matchedfs) Mkdir(name string, perm os.FileMode) error {
	name = filepath.Clean(name)
	name = filepath.ToSlash(name)
	name = strings.TrimRight(name, "/")

	parent := m.check(filepath.Dir(name) + "/")
	current := m.check(name + "/")
	switch {
	case !parent.Exists || parent.Excluded:
		return os.ErrNotExist
	case !parent.Writable:
		return os.ErrPermission
	case current.Excluded:
		return os.ErrPermission
	case current.Exists:
		return os.ErrExist
	case !current.Writable:
		return os.ErrPermission
	default:
		return m.fs.Mkdir(name, perm)
	}
}

func (m *matchedfs) MkdirAll(path string, perm os.FileMode) error {
	// normalize path so it is a/b/c/d
	path = filepath.Clean(path)
	path = filepath.ToSlash(path)
	path = strings.Trim(path, "/")

	// array of intermediate intermediates
	var intermediates []string
	for i, part := range strings.Split(path, "/") {
		if i > 0 {
			part = intermediates[i-1] + part
		}
		intermediates = append(intermediates, part + "/")
	}

	// check that each intermediate is not excluded and is writable
	for _, intermediate := range intermediates {
		check := m.check(intermediate)
		if check.Excluded || !check.Writable {
			return os.ErrPermission
		}
	}

	// make each intermediate
	for _, intermediate := range intermediates {
		err := m.Mkdir(intermediate, perm)
		if err != nil && !os.IsExist(err) {
			return err
		}
	}
	return nil
}

func (m *matchedfs) Name() string {
	return "matchedfs"
}

func (m *matchedfs) Open(name string) (afero.File, error) {
	return m.OpenFile(name, os.O_RDONLY, 0)
}

func (m *matchedfs) OpenFile(name string, flag int, perm os.FileMode) (afero.File, error) {
	parent := m.check(filepath.Dir(name))
	current := m.check(name)
	switch {
	case !parent.Exists || parent.Excluded:
		return nil, os.ErrNotExist
	case !parent.Readable:
		return nil, os.ErrPermission
	case current.Excluded:
		return nil, os.ErrNotExist
	case !current.Readable:
		return nil, os.ErrPermission
	default:
		return m.convertFile(name)(m.fs.OpenFile(name, flag, perm))
	}
}

func (m *matchedfs) Remove(name string) error {
	parent := m.check(filepath.Dir(name))
	current := m.check(name)
	switch {
	case !parent.Exists || parent.Excluded:
		return os.ErrNotExist
	case !parent.Writable:
		return os.ErrPermission
	case current.Excluded:
		return os.ErrNotExist
	case !current.Writable:
		return os.ErrPermission
	default:
		return m.fs.Remove(name)
	}
}

func (m *matchedfs) RemoveAll(path string) error {
	parent := m.check(filepath.Dir(path))
	current := m.check(path)
	switch {
	case !parent.Exists || parent.Excluded:
		return os.ErrNotExist
	case !parent.Writable:
		return os.ErrPermission
	case current.Excluded:
		return os.ErrNotExist
	case !current.Writable:
		return os.ErrPermission
	default:
		return m.fs.RemoveAll(path)
	}
}

func (m *matchedfs) Stat(name string) (os.FileInfo, error) {
	current := m.check(name)
	switch {
	case current.Excluded:
		return nil, os.ErrNotExist
	default:
		return m.convertStat(name, current)(m.fs.Stat(name))
	}
}

func (m *matchedfs) Chmod(name string, mode os.FileMode) error {
	current := m.check(name)
	switch {
	case current.Excluded:
		return os.ErrNotExist
	case !current.Writable:
		return os.ErrPermission
	default:
		return m.fs.Chmod(name, mode)
	}
}

func (m *matchedfs) Rename(src, dst string) error {
	from := m.check(src)
	to := m.check(dst)
	switch {
	case from.Excluded || !from.Exists:
		return os.ErrNotExist
	case !from.Writable:
		return os.ErrPermission
	case to.Excluded:
		return os.ErrNotExist
	case !to.Writable:
		return os.ErrPermission
	default:
		return m.fs.Rename(src, dst)
	}
}


func (m *matchedfs) Chtimes(name string, atime time.Time, mtime time.Time) error {
	current := m.check(name)
	switch {
	case current.Excluded:
		return os.ErrNotExist
	case !current.Writable:
		return os.ErrPermission
	default:
		return m.fs.Chtimes(name, atime, mtime)
	}
}

// New creates a new matchedfs
func New(underlyingFs afero.Fs) Fs {
	return &matchedfs{
		fs: underlyingFs,
	}
}
