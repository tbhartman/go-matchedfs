package matchedfs

import (
	"io"
	"os"
	"path/filepath"

	"github.com/spf13/afero"
)

type fileInfo struct {
	os.FileInfo
	Permission os.FileMode
}

func (f *fileInfo) Mode() os.FileMode {
	return f.Permission
}
func (f *fileInfo) Sys() interface{} {
	return nil
}

type file struct {
	afero.File
	fs       *matchedfs
	Path     string
	Check    checkResult
	dirnames []string
}

// these methods are passed through to the afero.File
//  - Name
//  - Close
//  - Read
//  - Seek
//  - Sync
//  - Truncate
//  - Write
//  - WriteAt
//  - ReadAt

func (s *file) Stat() (os.FileInfo, error) {
	return s.fs.Stat(s.Path)
}

func (s *file) Readdir(n int) (ret []os.FileInfo, err error) {
	// read from underlying until n matches the total to send (that haven't been excluded)
	var togo int = n
	var stats []os.FileInfo
	for togo > 0 || n < 1 {
		stats, err = s.File.Readdir(togo)
		togo = 0
		if err == io.EOF {
			break
		}
		if err != nil {
			return nil, err
		}
		for _, stat := range stats {
			check := s.fs.check(filepath.Join(s.Path, stat.Name()))
			if check.Excluded {
				togo++
				continue
			}
			stat, _ = s.fs.convertStat(s.Path, check)(stat, nil)
			ret = append(ret, stat)
		}
		if n < 1 {
			break
		}
	}
	if len(ret) == 0 && err == io.EOF {
		return nil, io.EOF
	}
	return ret, nil
}
func (s *file) Readdirnames(n int) (ret []string, err error) {
	infos, err := s.Readdir(n)
	if err != nil {
		return
	}
	for _, i := range infos {
		ret = append(ret, i.Name())
	}
	return
}
