package matchedfs_test

import (
	"fmt"
	"os"

	"github.com/spf13/afero"
	"gitlab.com/tbhartman/go-matchedfs"
)

type myMatcher struct {
	matchString []string
}
func (m *myMatcher) Match(name string) bool {
	for _, str := range m.matchString {
		if name == str {
			return true
		}
	}
	return false
}

func ExampleFs() {
	fs := matchedfs.New(afero.NewMemMapFs())
	fs.Mkdir("a", os.ModeDir | 0700)
	fs.Mkdir("b", os.ModeDir | 0700)

	fs.SetExcludeMatcher(&myMatcher{[]string{"a/"}})

	_, errA := fs.Stat("a")
	if errA == nil {
		fmt.Println("a exists")
	} else {
		fmt.Println("a does not exist")
	}
	_, errB := fs.Stat("b")
	if errB == nil {
		fmt.Println("b exists")
	} else {
		fmt.Println("b does not exist")
	}

	// Output:
	// a does not exist
	// b exists
}
