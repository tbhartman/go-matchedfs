package matchedfs_test

import (
	"os"
	"testing"

	"gitlab.com/tbhartman/go-matchedfs"

	"github.com/spf13/afero"
	"github.com/stretchr/testify/assert"

	"github.com/sabhiram/go-gitignore"
)


func TestGitIgnoreExclude(t *testing.T) {
	ufs := afero.NewMemMapFs()
	fs := matchedfs.New(ufs)

	ufs.Mkdir("b", os.ModeDir | 0700)
	ufs.Mkdir("bar", os.ModeDir | 0700)
	f, _ := ufs.Create("b/bar")
	f.WriteString("bar")
	f.Close()
	ufs.Chmod("b/bar", 0700)

	_, err := fs.Stat("b/bar")
	assert.NoError(t, err)

	fs.SetExcludePathMatcher(ignore.CompileIgnoreLines("bar/"))

	_, err = fs.Stat("bar")
	assert.Error(t, err)
	assert.True(t, os.IsNotExist(err))

	_, err = fs.Stat("b/bar")
	assert.NoError(t, err)

}
