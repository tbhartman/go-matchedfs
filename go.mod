module gitlab.com/tbhartman/go-matchedfs

go 1.15

require (
	github.com/sabhiram/go-gitignore v0.0.0-20201211210132-54b8a0bf510f
	github.com/spf13/afero v1.5.1
	github.com/stretchr/testify v1.6.1
)
